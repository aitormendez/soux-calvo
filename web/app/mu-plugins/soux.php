<?php
   /*
   Plugin Name: Soux
   Text Domain: soux
   Plugin URI:
   Description:
   Version: 1.0
   Author: Aitor
   Author URI: https://e451.net
   License: Private
   */
?>
<?php



// Register Custom Taxonomy
function personas_taxonomy() {

	$labels = array(
		'name'                       => _x( 'Personas', 'Taxonomy General Name', 'soux' ),
		'singular_name'              => _x( 'Persona', 'Taxonomy Singular Name', 'soux' ),
		'menu_name'                  => __( 'Personas', 'soux' ),
		'all_items'                  => __( 'Todas las personas', 'soux' ),
		'parent_item'                => __( 'Parent Item', 'soux' ),
		'parent_item_colon'          => __( 'Parent Item:', 'soux' ),
		'new_item_name'              => __( 'Nuevo nombre de persona', 'soux' ),
		'add_new_item'               => __( 'Añadir persona nueva', 'soux' ),
		'edit_item'                  => __( 'Editar persona', 'soux' ),
		'update_item'                => __( 'Actualizar persona', 'soux' ),
		'view_item'                  => __( 'Ver persona', 'soux' ),
		'separate_items_with_commas' => __( 'Separar personas con comas', 'soux' ),
		'add_or_remove_items'        => __( 'Añade o elimina personas', 'soux' ),
		'choose_from_most_used'      => __( 'Choose from the most used', 'soux' ),
		'popular_items'              => __( 'Personas populares', 'soux' ),
		'search_items'               => __( 'Buscar personas', 'soux' ),
		'not_found'                  => __( 'No encontrada', 'soux' ),
		'no_terms'                   => __( 'No items', 'soux' ),
		'items_list'                 => __( 'Lista de personas', 'soux' ),
		'items_list_navigation'      => __( 'Navegación lista de personas', 'soux' ),
	);
	$args = array(
		'labels'                     => $labels,
		'hierarchical'               => false,
		'public'                     => true,
		'show_ui'                    => true,
		'show_admin_column'          => true,
		'show_in_nav_menus'          => true,
		'show_tagcloud'              => true,
	);
	register_taxonomy( 'personas', array( 'post', ' carta' ), $args );

}
add_action( 'init', 'personas_taxonomy', 0 );

// Register Custom Post Type
function cartas_post_type() {

	$labels = array(
		'name'                  => _x( 'Cartas', 'Post Type General Name', 'soux' ),
		'singular_name'         => _x( 'Carta', 'Post Type Singular Name', 'soux' ),
		'menu_name'             => __( 'Cartas', 'soux' ),
		'name_admin_bar'        => __( 'Carta', 'soux' ),
		'archives'              => __( 'Archivo de cartas', 'soux' ),
		'attributes'            => __( 'Atributos de cartas', 'soux' ),
		'parent_item_colon'     => __( 'Carta padre:', 'soux' ),
		'all_items'             => __( 'Todas las cartas', 'soux' ),
		'add_new_item'          => __( 'Añadir nueva carta', 'soux' ),
		'add_new'               => __( 'Añadir nueva', 'soux' ),
		'new_item'              => __( 'Nueva carta', 'soux' ),
		'edit_item'             => __( 'Editar carta', 'soux' ),
		'update_item'           => __( 'Actualizar carta', 'soux' ),
		'view_item'             => __( 'Ver carta', 'soux' ),
		'view_items'            => __( 'Ver cartas', 'soux' ),
		'search_items'          => __( 'Buscar carta', 'soux' ),
		'not_found'             => __( 'Not found', 'soux' ),
		'not_found_in_trash'    => __( 'Not found in Trash', 'soux' ),
		'featured_image'        => __( 'Featured Image', 'soux' ),
		'set_featured_image'    => __( 'Set featured image', 'soux' ),
		'remove_featured_image' => __( 'Remove featured image', 'soux' ),
		'use_featured_image'    => __( 'Use as featured image', 'soux' ),
		'insert_into_item'      => __( 'Insertar en la carta', 'soux' ),
		'uploaded_to_this_item' => __( 'Uploaded to this item', 'soux' ),
		'items_list'            => __( 'Items list', 'soux' ),
		'items_list_navigation' => __( 'Items list navigation', 'soux' ),
		'filter_items_list'     => __( 'Filter items list', 'soux' ),
	);
	$args = array(
		'label'                 => __( 'Carta', 'soux' ),
		'description'           => __( 'Correspondecia de la famila Soux Calvo', 'soux' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'editor', 'thumbnail' ),
		'taxonomies'            => array( 'personas' ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 5,
		'menu_icon'             => 'dashicons-email-alt',
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => true,
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'capability_type'       => 'page',
	);
	register_post_type( 'carta', $args );

}
add_action( 'init', 'cartas_post_type', 0 );


// Register Custom Post Type
function adjuntos() {

	$labels = array(
		'name'                  => _x( 'Adjuntos', 'Post Type General Name', 'soux' ),
		'singular_name'         => _x( 'Adjunto', 'Post Type Singular Name', 'soux' ),
		'menu_name'             => __( 'Adjuntos', 'soux' ),
		'name_admin_bar'        => __( 'Adjuntos', 'soux' ),
		'archives'              => __( 'Archivo adjunto', 'soux' ),
		'attributes'            => __( 'Atributo adjunto', 'soux' ),
		'parent_item_colon'     => __( 'Adjunto padre:', 'soux' ),
		'all_items'             => __( 'Todos los adjuntos', 'soux' ),
		'add_new_item'          => __( 'Añadir nuevo adjunto', 'soux' ),
		'add_new'               => __( 'Añadir nuevo', 'soux' ),
		'new_item'              => __( 'Nuevo adjunto', 'soux' ),
		'edit_item'             => __( 'Editar adjunto', 'soux' ),
		'update_item'           => __( 'Actualizar adjunto', 'soux' ),
		'view_item'             => __( 'Ver adjunto', 'soux' ),
		'view_items'            => __( 'Ver adjuntos', 'soux' ),
		'search_items'          => __( 'Buscar adjunto', 'soux' ),
		'not_found'             => __( 'No encontrado', 'soux' ),
		'not_found_in_trash'    => __( 'NNo encontrado en la papelera', 'soux' ),
		'featured_image'        => __( 'Featured Image', 'soux' ),
		'set_featured_image'    => __( 'Set featured image', 'soux' ),
		'remove_featured_image' => __( 'Remove featured image', 'soux' ),
		'use_featured_image'    => __( 'Use as featured image', 'soux' ),
		'insert_into_item'      => __( 'Insert into item', 'soux' ),
		'uploaded_to_this_item' => __( 'Uploaded to this item', 'soux' ),
		'items_list'            => __( 'Items list', 'soux' ),
		'items_list_navigation' => __( 'Items list navigation', 'soux' ),
		'filter_items_list'     => __( 'Filter items list', 'soux' ),
	);
	$args = array(
		'label'                 => __( 'Adjunto', 'soux' ),
		'description'           => __( 'Archivos para mostrar en la web', 'soux' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'excerpt', ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 5,
		'menu_icon'             => 'dashicons-format-aside',
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => true,
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'capability_type'       => 'page',
	);
	register_post_type( 'adjunto', $args );

}
add_action( 'init', 'adjuntos', 0 );

// Register Custom Post Type
function enlaces() {

	$labels = array(
		'name'                  => _x( 'Enlaces', 'Post Type General Name', 'soux' ),
		'singular_name'         => _x( 'Enlace', 'Post Type Singular Name', 'soux' ),
		'menu_name'             => __( 'Enlaces', 'soux' ),
		'name_admin_bar'        => __( 'Enlace', 'soux' ),
		'archives'              => __( 'Archivo de enlaces', 'soux' ),
		'attributes'            => __( 'Atrbutos de enlaces', 'soux' ),
		'parent_item_colon'     => __( 'Enlace padre:', 'soux' ),
		'all_items'             => __( 'Todos los enlaces', 'soux' ),
		'add_new_item'          => __( 'Añadir nuevo enlace', 'soux' ),
		'add_new'               => __( 'Añadir nuevo', 'soux' ),
		'new_item'              => __( 'Nuevo enlace', 'soux' ),
		'edit_item'             => __( 'Editar enlace', 'soux' ),
		'update_item'           => __( 'Actualizar enlace', 'soux' ),
		'view_item'             => __( 'Ver enlace', 'soux' ),
		'view_items'            => __( 'Ver enlace', 'soux' ),
		'search_items'          => __( 'Buscar enlace', 'soux' ),
		'not_found'             => __( 'Not found', 'soux' ),
		'not_found_in_trash'    => __( 'Not found in Trash', 'soux' ),
		'featured_image'        => __( 'Featured Image', 'soux' ),
		'set_featured_image'    => __( 'Set featured image', 'soux' ),
		'remove_featured_image' => __( 'Remove featured image', 'soux' ),
		'use_featured_image'    => __( 'Use as featured image', 'soux' ),
		'insert_into_item'      => __( 'Insert into item', 'soux' ),
		'uploaded_to_this_item' => __( 'Uploaded to this item', 'soux' ),
		'items_list'            => __( 'Items list', 'soux' ),
		'items_list_navigation' => __( 'Items list navigation', 'soux' ),
		'filter_items_list'     => __( 'Filter items list', 'soux' ),
	);
	$args = array(
		'label'                 => __( 'Enlace', 'soux' ),
		'description'           => __( 'Enlaces a mostrar en la página de enlaces', 'soux' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'excerpt', ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 5,
    'menu_icon'             => 'dashicons-admin-links',
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => true,
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'capability_type'       => 'page',
	);
	register_post_type( 'enlaces', $args );

}
add_action( 'init', 'enlaces', 0 );


// eliminar "cartas" del título

add_filter( 'get_the_archive_title', function ( $title ) {
    if( is_tax('personas') ) {
        $title = single_cat_title( '', false );
    }
    return $title;
});


/*
TAMAÑO PERSONALIZADO MINIATURAS GALERÍAS.
Register shortcode_atts_gallery filter callback
http://mekshq.com/change-image-thumbnail-size-in-wordpress-gallery/
*/
add_filter( 'shortcode_atts_gallery', 'soux_gallery_atts', 10, 3 );
function soux_gallery_atts( $output, $pairs, $atts ) {
    $output['size'] = 'thumbnail';
return $output;
}

/*
Ordenar cartas por fecha
*/
add_action( 'pre_get_posts', 'cartas_get_posts' );

function cartas_get_posts( $query ) {
	// Check if on frontend and main query is modified
	if( is_tax('personas') && $query->is_main_query() ) {
    $query->set('meta_key', 'fecha');
    $query->set('orderby', 'meta_value_num');
    $query->set('order', 'ASC');
    }
    return $query;
}



//TinyMCE
function add_style_select_buttons( $buttons ) {
    array_unshift( $buttons, 'styleselect' );
    return $buttons;
}
// Register our callback to the appropriate filter
add_filter( 'mce_buttons_2', 'add_style_select_buttons' );

//add custom styles to the WordPress editor
function my_custom_styles( $init_array ) {

    $style_formats = array(
        // These are the custom styles
        array(
            'title' => 'Flow text',
            'block' => 'p',
            'classes' => 'flow-text',
            'wrapper' => false,
        ),
    );
    // Insert the array, JSON ENCODED, into 'style_formats'
    $init_array['style_formats'] = json_encode( $style_formats );

    return $init_array;

}
// Attach callback to 'tiny_mce_before_init'
add_filter( 'tiny_mce_before_init', 'my_custom_styles' );

?>
