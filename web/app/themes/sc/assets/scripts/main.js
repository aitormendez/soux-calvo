/* ========================================================================
 * DOM-based Routing
 * Based on http://goo.gl/EUTi53 by Paul Irish
 *
 * Only fires on body classes that match. If a body class contains a dash,
 * replace the dash with an underscore when adding it to the object below.
 *
 * .noConflict()
 * The routing is enclosed within an anonymous function so that you can
 * always reference jQuery with $, even when in .noConflict() mode.
 * ======================================================================== */

(function($) {

  // Use this variable to set up the common and page specific functions. If you
  // rename this variable, you will also need to rename the namespace below.
  var Sage = {
    // All pages
    'common': {
      init: function() {
        // JavaScript to be fired on all pages


        $( document ).ready(function(){
          // inicializar barra lateral movil materializecss
          $(".button-collapse").sideNav();

        });


      },
      finalize: function() {
        // JavaScript to be fired on all pages, after page specific JS is fired
      }
    },
    // Home page
    'home': {
      init: function() {
        // JavaScript to be fired on the home page

        // scroll animado botón dedo
        $(document).ready(function(){
        	$('a[href^="#"]').on('click',function (e) {
        	    e.preventDefault();

        	    var target = this.hash;
        	    var $target = $(target);

        	    $('html, body').stop().animate({
        	        'scrollTop': $target.offset().top
        	    }, 900, 'swing', function () {
        	        window.location.hash = target;
        	    });
        	});


            // owl caroussel
            $('.owl-carousel').owlCarousel({
                margin:50,
                responsive:{
                    0:{
                        items:2
                    },
                    400:{
                        items:3
                    },
                    700:{
                        items:4
                    },
                    1000:{
                        items:5
                    },
                    1300:{
                        items:6
                    },
                    1600:{
                        items:7
                    }
                }
            });

            // Waypoints cabecera
            var viewportWidth = $(window).width();
            if (viewportWidth >= 600) {
              var waypoint = new Waypoint({
                element: document.getElementById('trigger'),
                handler: function(direction) {
                  $( '.banner nav' ).toggleClass( "peq" );
                }
              });
            }
        });





      },
      finalize: function() {
        // JavaScript to be fired on the home page, after the init JS
      }
    },
    // About us page, note the change from about-us to about_us.
    'about_us': {
      init: function() {
        // JavaScript to be fired on the about us page
      }
    },
    'single_carta': {
      init: function() {
        // JavaScript to be fired on single page

        $(document).ready(function(){
          $('.gallery').justifiedGallery({
            rowHeight : 150,
            margins : 3
          }).on('jg.complete', function(){
            $(this).lightGallery({
              thumbnail: true
            });
          });
        });




      }
    },
    'galeria_de_fotos': {
      init: function() {
        // JavaScript to be fired on single page



      $(document).ready(function(){
        $('.gallery').justifiedGallery({
          rowHeight : 100,
          margins : 3,
          cssAnimation: true
        }).on('jg.complete', function(){
          $(this).lightGallery({
            thumbnail: true
          });
        });
      });



      }
    },
    'enlaces_y_archivos': {
      init: function() {
        // JavaScript to be fired on page

        $(document).ready(function(){
          $('ul.tabs').tabs();
        });



      }
    }
  };

  // The routing fires all common scripts, followed by the page specific scripts.
  // Add additional events for more control over timing e.g. a finalize event
  var UTIL = {
    fire: function(func, funcname, args) {
      var fire;
      var namespace = Sage;
      funcname = (funcname === undefined) ? 'init' : funcname;
      fire = func !== '';
      fire = fire && namespace[func];
      fire = fire && typeof namespace[func][funcname] === 'function';

      if (fire) {
        namespace[func][funcname](args);
      }
    },
    loadEvents: function() {
      // Fire common init JS
      UTIL.fire('common');

      // Fire page-specific init JS, and then finalize JS
      $.each(document.body.className.replace(/-/g, '_').split(/\s+/), function(i, classnm) {
        UTIL.fire(classnm);
        UTIL.fire(classnm, 'finalize');
      });

      // Fire common finalize JS
      UTIL.fire('common', 'finalize');
    }
  };

  // Load Events
  $(document).ready(UTIL.loadEvents);

})(jQuery); // Fully reference jQuery after this point.
