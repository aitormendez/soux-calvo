<?php
$titulo = get_the_archive_title();
$term_object = get_queried_object();
$size = 'thumbnail';
$foto = get_field('fotopersona', 'personas'.'_'.$term_object->term_id);
$avatar = get_field('avatar', 'personas'.'_'.$term_object->term_id);
$foto_url = $foto['sizes'][$size];
$width = $foto['sizes'][$size . '-width'];
$height = $foto['sizes'][$size . '-height']; ?>

<div class="container">
  <header>
  <?php
  if (! empty($foto)) { ?>
    <img class="circle" src="<?php echo $foto_url; ?>" alt="<?php echo $term_object->name; ?>" width="<?php echo $width; ?>" height="<?php echo $height; ?>" />
  <?php } else { ?>

      <div class="sin-avatar circle s s-pluma-<?php echo $avatar; ?>"></div>
  <?php } ?>
  <h1><?php echo $titulo; ?></h1>
  </header>

  <div class="contenido">
  <?php while (have_posts()) : the_post();

  ?>
    <?php get_template_part('templates/content-personas', get_post_type() != 'post' ? get_post_type() : get_post_format()); ?>
  <?php endwhile; ?>
  </div>
  <?php the_posts_navigation(); ?>
</div>
