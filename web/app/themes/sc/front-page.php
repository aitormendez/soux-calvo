<div id="trigger"></div>
<section class="front-bg">
  <a class="abajo-1" href="#personas-destacadas" role="button">
    <span class="s s-abajo"></span>
    <span class="s s-abajo-fill"></span>
  </a>
</section>
<section class="existe-la-magia" id="existe-la-magia">
<div class="container flow-text">


  <?php $args = array(
    'pagename' => 'existe-la-magia',
  );
  $query_intro = new WP_Query( $args );
  if ( $query_intro->have_posts() ) {
    while ( $query_intro->have_posts() ) {
      $query_intro->the_post();
      the_content();
    }
  } else {
    echo 'Falta crear lapágina "¿Existe la magia?"';
  }
  wp_reset_postdata(); ?>


</div>
</section>

<section class="personas-destacadas" id="personas-destacadas">
  <div class="owl-carousel owl-theme">
  <?php
  $args = array(
        'hide_empty' => 0,
  );

  $terms = get_terms( 'personas', $args );
  if ( ! empty( $terms ) && ! is_wp_error( $terms ) ) {
      foreach ( $terms as $term ) {
        $image = get_field('fotopersona', $term);
        $portada = get_field('portada', $term);

        if( !empty($image) && $portada == '1' ):

        // thumbnail
        $size = 'thumbnail';
        $thumb = $image['sizes'][ $size ];
        $width = $image['sizes'][ $size . '-width' ];
        $height = $image['sizes'][ $size . '-height' ];
        ?>
          <a href="<?php echo get_term_link( $term ); ?>" title="<?php echo $term->name; ?>">
            <img class="circle" src="<?php echo $thumb; ?>" alt="<?php echo $term->name; ?>" width="<?php echo $width; ?>" height="<?php echo $height; ?>" />
            <h3><?php echo $term->name; ?></h3>
          </a>
        <?php elseif (empty($image) && $portada == '1') :
          $avatar = get_field('avatar', $term);?>
          <a href="<?php echo get_term_link( $term ); ?>" title="<?php echo $title; ?>">
            <span class="sin-avatar circle s s-pluma-<?php echo $avatar; ?>"></span>
            <h3><?php echo $term->name; ?></h3>
          </a>
        <?php endif; ?>

      <?php }
  }
  ?>

  </div>
</section>
