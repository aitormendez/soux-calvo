<?php
/**
 * Template Name: Enlaces y archivos
 */
?>
<div class="container">
	<div class="col arbol">
		<a href="http://www.macfamilytree.com/Inmaculada%20Barral/SouxCalvo/index.html" alt="arbol genealógico" target="_blank">
			<span class="s s-arbol s-4x"></span>
			<span>Ver el árbol genealógico</span>
		</a>
	</div>
<?php //query adjuntos
$args = array(
	'post_type' => array( 'adjunto' ),
);
$query_adjuntos = new WP_Query( $args );
?>

<?php //query enlaces
$args = array(
	'post_type' => array( 'enlaces' ),
);
$query_enlaces = new WP_Query( $args );
?>


<div class="row">
    <div class="cabecera col s12">
      <ul class="tabs">
        <li class="tab col s6"><a href="#enlaces">Enlaces</a></li>
        <li class="tab col s6"><a href="#archivos">Archivos</a></li>
      </ul>
    </div>
    <div id="enlaces">
      <ul>
        <?php if ( $query_enlaces->have_posts() ) {
          while ( $query_enlaces->have_posts() ) {
            $query_enlaces->the_post();
            $enlace = get_field('url');
            $nombre = get_the_title(); ?>
            <li>
              <a href="<?php echo $enlace; ?>" title="<?php echo $nombre; ?>" target="_blank">
                <?php echo $nombre; ?>
              </a>
							<?php  if (!empty($post->post_excerpt)) { ?>
								<div class="descripcion">
	                <?php the_excerpt(); ?>
	              </div>
							<?php }?>

            </li>
          <?php
          }
        } else {
          echo "no hay posts del tipo enlace";
        }
        wp_reset_postdata(); ?>
      </ul>
    </div>
    <div id="archivos">
      <ul>
        <?php if ( $query_adjuntos->have_posts() ) {
        	while ( $query_adjuntos->have_posts() ) {
        		$query_adjuntos->the_post();
            $file = get_field('adjunto');
            if ($file) {
              $url = $file['url'];
            	$title = $file['title'];
            	$caption = $file['caption'];
              $titulo = get_the_title(); ?>
              <li>
                <a href="<?php echo $url; ?>" title="<?php echo $titulo; ?>">
                  <span><?php echo $titulo; ?></span>
                </a>
                <div class="descripcion">
                  <?php the_excerpt(); ?>
                </div>
              </li>
            <?php }
        	}
        } else {
        	echo "no hay posts del tipo archivo adjunto";
        }
        wp_reset_postdata();?>
      </ul>
    </div>
  </div>
</div>
