<?php
/**
 * Template Name: Las cartas
 */
?>
<div class="container">

<?php
$args = array(
      'hide_empty' => 0,
);
$terms = get_terms( 'personas', $args );
if ( ! empty( $terms ) && ! is_wp_error( $terms ) ) {
    wp_reset_query();
    foreach ( $terms as $term ) { ?>

      <article class="persona">
        <header>
          <?php $image = get_field('fotopersona', $term);
          $portada = get_field('portada', $term);
          if( !empty($image)):
            $size = 'thumbnail';
            $thumb = $image['sizes'][ $size ];
            $width = $image['sizes'][ $size . '-width' ];
            $height = $image['sizes'][ $size . '-height' ]; ?>

              <a href="<?php echo get_term_link( $term ); ?>" title="<?php echo $term->name; ?>">
                <img class="circle" src="<?php echo $thumb; ?>" alt="<?php echo $term->name; ?>" width="<?php echo $width; ?>" height="<?php echo $height; ?>" />
                <h1><?php echo $term->name; ?></h1>
              </a>

            <?php elseif (empty($image) && $portada == '1') :
              $avatar = get_field('avatar', $term);?>
              <a href="<?php echo get_term_link( $term ); ?>" title="<?php echo $title; ?>">
                <span class="sin-avatar circle s s-pluma-<?php echo $avatar; ?>"></span>
                <h1><?php echo $term->name; ?></h1>
              </a>
          <?php endif; ?>
        </header>

      <div class="contenido">
      <?php $args = array(
      	'post_type' => array( 'carta' ),
        'meta_key'	=> 'fecha',
      	'orderby'	=> 'meta_value_num',
      	'order'		=> 'ASC',
      	'tax_query' => array(
      		array(
      			'taxonomy' => 'personas',
            'field'    => 'slug',
            'terms'    => $term->slug,
      		),
      	),
      );
      $query_personas = new WP_Query( $args );

      if ( $query_personas->have_posts() ) {
      	while ( $query_personas->have_posts() ) {
      		$query_personas->the_post(); ?>

          <?php get_template_part('templates/content-personas', get_post_type() != 'post' ? get_post_type() : get_post_format()); ?>

      	<?php }
      }
      // Restore original Post Data
      wp_reset_postdata(); ?>
      </div>
    </article> <?php
  }
} ?>
</div>
