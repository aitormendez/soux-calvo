<a href="<?php the_permalink();?>" class="carta">
  <?php the_post_thumbnail('thumbnail');

  $fecha = strtotime(get_field('fecha'));
  if (empty($fecha)) {
    $fecha = "Sin fecha";
  }  else {
      $fecha = date_i18n( 'l d F, Y', $fecha );
  } ?>
  <div class="meta">
    <span class="titulo"><?php the_title();?></span>
    <span class="fecha"><?php echo $fecha; ?></span>
  </div>
</a>
