<header class="banner">
  <nav class="z-depth-0">
      <div class="nav-wrapper">
        <div class="logo-wrapper">
            <div class="top-line"></div>
            <a class="brand" href="<?= esc_url(home_url('/')); ?>">
              <span class="fundi-font nombre-a">VIAJE A NUESTRO</span>
              <span class="viejuno-font nombre-b">PASADO COMÚN</span>
              <span class="s s-pluma"></span>
              <span class="fundi-font subtitulo"><?php echo get_bloginfo( 'description' ); ?></span>
            </a>
            <div class="bottom-line"></div>

        </div>

        <a href="#" data-activates="movil" class="button-collapse hide-on-med-and-up">
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </a>
          <?php
          if (has_nav_menu('primary_navigation')) :
            wp_nav_menu(['theme_location' => 'primary_navigation', 'menu_class' => 'principal hide-on-small-only right']);
          endif;
          ?>
          <?php
          if (has_nav_menu('primary_navigation')) :
            wp_nav_menu(['theme_location' => 'side_navigation', 'menu_class' => 'side-nav', 'menu_id' => 'movil']);
          endif;
          ?>
      </div>
    </nav>
</header>
