<div class="meta">
  <?php if (is_singular('carta')) {
    $fecha = strtotime(get_field('fecha'));
    if (empty($fecha)) {
      $fecha = "Sin fecha";
    }  else {
        $fecha = date_i18n( 'l d F, Y', $fecha );
    }
 ?>
  <time>
    <?php echo $fecha; ?>
  </time>
  <p>
    <?php $personas = get_the_term_list( $post->ID, 'personas', '', ', ' );
    echo $personas?>
  </p>

  <?php } else { ?>
    <time class="updated" datetime="<?= get_post_time('c', true); ?>"><?= get_the_date(); ?></time>
    <p class="byline author vcard"><?= __('By', 'sage'); ?> <a href="<?= get_author_posts_url(get_the_author_meta('ID')); ?>" rel="author" class="fn"><?= get_the_author(); ?></a></p>
  <?php }?>
</div>
