<header class="banner">
  <div class="container-fluid">

    <nav class="navbar navbar-toggleable-sm navbar-default navbar-static">

      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo01" aria-controls="navbarToggler" aria-expanded="false" aria-label="Toggle navigation">
        <span class="sr-only">Toggle navigation</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
      </button>
      <div class="card-panel hoverable"> Hoverable Card Panel</div>

      <a class="brand" href="<?= esc_url(home_url('/')); ?>">
        <span class="fundi-font nombre-a">VIAJE A NUESTRO</span>
        <span class="viejuno-font nombre-b">PASADO COMÚN</span>
        <span class="s s-pluma"></span>
        <span class="fundi-font subtitulo"><?php echo get_bloginfo( 'description' ); ?></span>
      </a>
      <div class="collapse navbar-collapse" id="navbarToggler">
        <?php
        if (has_nav_menu('primary_navigation')) :
          wp_nav_menu(['theme_location' => 'primary_navigation', 'menu_class' => 'navbar-nav mr-auto mt-2 mt-lg-0']);
        endif;
        ?>
        <ul class="navbar-nav mr-auto mt-2 mt-lg-0">
          <li class="nav-item active">
            <a class="nav-link" href="#">Home <span class="sr-only">(current)</span></a>
          </li>
        </ul>
      </div>
    </nav>
  </div>
</header>
