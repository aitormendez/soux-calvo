<?php

namespace Roots\Sage\Assets;

/**
 * Get paths for assets
 */
class JsonManifest {
  private $manifest;

  public function __construct($manifest_path) {
    if (file_exists($manifest_path)) {
      $this->manifest = json_decode(file_get_contents($manifest_path), true);
    } else {
      $this->manifest = [];
    }
  }

  public function get() {
    return $this->manifest;
  }

  public function getPath($key = '', $default = null) {
    $collection = $this->manifest;
    if (is_null($key)) {
      return $collection;
    }
    if (isset($collection[$key])) {
      return $collection[$key];
    }
    foreach (explode('.', $key) as $segment) {
      if (!isset($collection[$segment])) {
        return $default;
      } else {
        $collection = $collection[$segment];
      }
    }
    return $collection;
  }
}

function asset_path($filename) {
  $dist_path = get_template_directory_uri() . '/dist/';
  $directory = dirname($filename) . '/';
  $file = basename($filename);
  static $manifest;

  if (empty($manifest)) {
    $manifest_path = get_template_directory() . '/dist/' . 'assets.json';
    $manifest = new JsonManifest($manifest_path);
  }

  if (array_key_exists($file, $manifest->get())) {
    return $dist_path . $directory . $manifest->get()[$file];
  } else {
    return $dist_path . $directory . $file;
  }
}

// enqueues míos

function add_scripts() {
  wp_register_script('lightgallery', asset_path('scripts/lightgallery.js'), [], null, true);
  wp_register_script('lg-thumbnail', asset_path('scripts/lg-thumbnail.js'), ['lightgallery'], null, true);
  wp_register_script('justifiedGallery', asset_path('scripts/justifiedGallery.js'), ['lightgallery'], null, true);
  wp_enqueue_script('lightgallery');
  wp_enqueue_script('lg-thumbnail');
  wp_enqueue_script('justifiedGallery');
}
add_action('wp_enqueue_scripts',  __NAMESPACE__ . '\\add_scripts');

function add_styles() {
  wp_enqueue_style('lightgallery', asset_path('styles/lightgallery.css'), false, null);
  wp_enqueue_style('justifiedGallery', asset_path('styles/justifiedGallery.css'), false, null);
}
add_action('wp_enqueue_scripts',  __NAMESPACE__ . '\\add_styles');
